import { HttpClient } from '@angular/common/http';
import { HtmlParser } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Menu } from '../interfaces/menu';
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  //Damos acceso para los menu del usuario que entre
  constructor(private http: HttpClient) { }
  getMenu():Observable<Menu[]> {
    return this.http.get<Menu[]>('./assets/data/menu.json');
  }
}
