import { Usuario } from './../interfaces/usuario';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  listUsuarios: Usuario[] = [
    {nombre: 'Byron Alexander', apellido: 'Santacruz Saltos', telefono: 981307530, correo: 'byron.santacruz2002@gmail.com',rol:'Administrador'},
    {nombre: 'Nohelia Alexandra', apellido: 'Santacruz Saltos', telefono: 999809041, correo: 'Nohelia@gmail.com',rol:'Estudiante'},
    {nombre: 'Patricia Alexandra ', apellido: 'Quiroz Palma', telefono: 981303040, correo: 'patricia@gmail.com',rol:'Docente'},
    {nombre: 'Narcisa Anahy ', apellido: 'Santacruz Saltos', telefono: 990303040, correo: 'narcisa@gmail.com',rol:'Estudiante'},
    {nombre: 'Flerida Alexandra ', apellido: 'Saltos Resabala', telefono: 990304040, correo: 'flerida@gmail.com',rol:'Docente'},
    {nombre: 'LLEYTON JAIME  ', apellido: 'ACOSTA PINCAY', telefono: 981307530, correo: 'e1314978618@live.uleam.edu.ec',rol:'Administrador'},
    {nombre: 'BRYAN JESUS ', apellido: 'ANCHUNDIA MACIAS', telefono: 999809041, correo: 'e1315074052@live.uleam.edu.ec',rol:'Estudiante'},
    {nombre: 'CARLOS DANIEL', apellido: 'ANDRADE MONTESDEOCA', telefono: 981303040, correo: 'e1309784369@live.uleam.edu.ec',rol:'Docente'},
    {nombre: 'ANA BELEN ', apellido: 'ANDRADE ORTEGA', telefono: 990303040, correo: 'e1314208107@live.uleam.edu.ec',rol:'Estudiante'},
    {nombre: 'ANTHONY ROGERS', apellido: 'ARTEAGA MEZA', telefono: 990304040, correo: 'e1315669968@live.uleam.edu.ec',rol:'Docente'}
  ];

  constructor(private http:HttpClient) { }
  getUsuario(){
    return this.listUsuarios.slice();
  }
  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }
  agregarUsuario(usuario:Usuario){
    this.listUsuarios.unshift(usuario);
  }
}
