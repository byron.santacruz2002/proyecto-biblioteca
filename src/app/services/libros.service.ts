import { Injectable } from '@angular/core';
import { Libros } from '../interfaces/libros';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  listLibros: Libros[] = [
    {libro: 'Eletricidad Basica', escritor: 'Jose Luis Bosque', categoria: 'Electrónicos', ano:'2021-12-21',pdf:'C:\fakepath\ Electricidad Básica autor FTS, Universidad Nacional de La Plata.pdf'},
    {libro: 'Eletricidad Basica', escritor: 'Maria Belen', categoria: 'Electrónicos', ano:'2020-05-21',pdf:'C:\fakepath\ElectricidadBasica.pdf'},
    {libro: 'APLICACIONES WEB I', escritor: 'Byron Santacruz', categoria: 'Tecnologia', ano:'2022-08-11',pdf:'C:\fakepath\PROYECTO PRIMER PARCIAL.pdf'},
    {libro: 'APLICACIONES WEB I', escritor: 'Byron Santacruz', categoria: 'Tecnologia', ano:'2022-10-11',pdf:'C:\fakepath\TAREA 2 PROYECTO.pdf'},
    {libro: 'APLICACIONES WEB I', escritor: 'Byron Santacruz', categoria: 'Tecnologia', ano:'2022-10-18',pdf:'C:\fakepath\TAREA 3 PROYECTO.pdf'},
    {libro: 'APLICACIONES WEB I', escritor: 'Byron Santacruz', categoria: 'Tecnologia', ano:'2022-10-25',pdf:'C:\fakepath\TAREA 4 PROYECTO.pdf'},
    {libro: 'APLICACIONES WEB I', escritor: 'Byron Santacruz', categoria: 'Tecnologia', ano:'2022-12-01',pdf:'C:\fakepath\TAREA 5 PROYECTO.pdf'}
    
  ];
  constructor(private http:HttpClient) { }
  getLibros(){
    return this.listLibros.slice();
  }
  eliminarLibro(index:number){
    this.listLibros.splice(index,1);
  }
  agregarLibro(libros:Libros){
    this.listLibros.unshift(libros);
  }
}