import { Noticia } from './../interfaces/noticias';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  listNoticia: Noticia[] = [
    {nombre: 'CARLOS DANIEL ANDRADE MONTESDEOCA', descripcion: 'Reservo el libro de HTML5', correo: 'e1309784369@live.uleam.edu.ec', fecha:'01/11/2022'},
    {nombre: 'BRYAN JESUS ANCHUNDIA MACIAS', descripcion: 'Reservo el libro de HTML5', correo: 'e1315074052@live.uleam.edu.ec', fecha:'01/11/2022'},
    {nombre: 'LLEYTON JAIME ACOSTA PINCAY', descripcion: 'Reservo el libro de HTML5', correo: 'e1314978618@live.uleam.edu.ec', fecha:'01/11/2022'},
    {nombre: 'Byron Alexander Santacruz Saltos', descripcion: 'Reservo el libro de HTML5', correo: 'byron.santacruz2002@gmail.com', fecha:'01/11/2022'},
    {nombre: 'ANA BELEN ANDRADE ORTEGA', descripcion: 'Reservo el libro de HTML5', correo: 'e1314208107@live.uleam.edu.ec', fecha:'01/11/2022'},
    {nombre: 'ANTHONY ROGERS ARTEAGA MEZA', descripcion: 'Reservo el libro de HTML5', correo: 'e1315669968@live.uleam.edu.ec', fecha:'01/11/2022'}
    
  ];
  constructor(private http:HttpClient) { }
  getNoticia(){
    return this.listNoticia.slice();
  }
  eliminarNoticia(index:number){
    this.listNoticia.splice(index,1);
  }
  agregarNoticia(noticia:Noticia){
    this.listNoticia.unshift(noticia);
  }
}