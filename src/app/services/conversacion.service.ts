import { Injectable } from '@angular/core';
import { Conversacion } from '../interfaces/conversacion';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ConversacionService {
  listConversacion: Conversacion[] = [
    {fecha: '01/12/2002', usuario: 'Jose Luis Bosque', comentario: 'Me gusta mucho esta pagina'},
    {fecha: '01/12/2003', usuario: 'Byron Santacruz', comentario: 'Puede ofrece un libro mejor de ciencia'},
    {fecha: '01/12/2004', usuario: 'Maria Jose', comentario: 'Hay un libro de Hackear Base de datos'},
    {fecha: '01/12/2015', usuario: 'Jose Ponce', comentario: 'Puedo ayudar en algo'},
    {fecha: '01/12/2020', usuario: 'Karen Jakerine', comentario: 'Saludo a todos'},
    {fecha: '01/12/2020', usuario: 'Nohelia Santacruz', comentario: 'Hola que tal'},
    {fecha: '01/12/2020', usuario: 'Narcisa Santacruz', comentario: 'Muy bien y ustedes?'},
  ];
  constructor(private http:HttpClient) { }
  getConversacion(){
    return this.listConversacion.slice();
  }
  eliminarConversacion(index:number){
    this.listConversacion.splice(index,1);
  }
  agregarConversacion(conversacion:Conversacion){
    this.listConversacion.unshift(conversacion);
  }
}