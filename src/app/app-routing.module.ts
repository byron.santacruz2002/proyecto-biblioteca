import { RecuperarPasswordComponent } from './Componentes/recuperar-password/recuperar-password.component';
import { CrearCuentaComponent } from './Componentes/crear-cuenta/crear-cuenta.component';
import { LoginComponent } from './Componentes/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  //Caundo el usaruio pone una ruta vacia
  {path:'', redirectTo:'login',pathMatch:'full'},
  //Me direcciona al login
  {path: 'login', component: LoginComponent },
  //Me direciona a crear cuenta
  {path: 'crear-cuenta', component:CrearCuentaComponent },
  //Me direciona a recuperar contraseña
  {path: 'recuperar-password', component:RecuperarPasswordComponent },
  //Me direciona a la pagina
  {path:'dashboard', loadChildren: () => import('./Componentes/dashboard/dashboard.module').then(x => x.DashboardModule)},
  //No puede haceder a otra direccion
  {path: '**',redirectTo:'login',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
