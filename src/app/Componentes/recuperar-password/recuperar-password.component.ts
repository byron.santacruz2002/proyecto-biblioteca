import { Router} from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireAuth } from '@angular/fire/compat/auth'

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.component.html',
  styleUrls: ['./recuperar-password.component.css']
})
export class RecuperarPasswordComponent {
  recuperarUsuario:FormGroup;
  loading:boolean = false;

  constructor(
    private fb:FormBuilder,
    private afAuth:AngularFireAuth,
    private snackBar: MatSnackBar, 
    private router: Router){
      this.recuperarUsuario = this.fb.group({
        correo: ['',Validators.required]
      })
    }

  ngOnInit():void{
  }
  recuperar(){
    const email = this.recuperarUsuario.value.correo;
    this.loading=true;
    this.afAuth.sendPasswordResetEmail(email).then(() => {
      this.snackBar.open('Le enviamos un correo para restablecer su contraseña','😎📫', {
        duration:7000,
        horizontalPosition:'center',
        verticalPosition:'top',
      });
      this.router.navigate(['/login']);
    }).catch((error)=> {
      this.loading=false;
      this.snackBar.open(this.firebaseerror(error.code),'🤔', {
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom',
      });
    })
  }
  firebaseerror(code:string){
    switch(code){
      case'auth/user-not-found':
        return 'Este usuario no existe'
      case'auth/invalid-email':
        return 'Correo invalido'
      default:
        return 'Error desconocido'
    }
  }
}
