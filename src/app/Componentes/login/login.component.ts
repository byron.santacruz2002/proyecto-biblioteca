import { Router} from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireAuth } from '@angular/fire/compat/auth'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginUsuario: FormGroup;
  loading: boolean = false;
  
  constructor(
    private fb:FormBuilder,
    private afAuth:AngularFireAuth,
    private snackBar: MatSnackBar, 
    private router: Router){
      this.loginUsuario = this.fb.group({
        email:['',Validators.required],
        password:['',Validators.required],
      })
     }

  ngOnInit():void{}

  login(){
    const email = this.loginUsuario.value.email;
    const password = this.loginUsuario.value.password;
    this.loading=true;
    this.afAuth.signInWithEmailAndPassword(email,password).then((user) => {
      console.log(user);
      setTimeout(() => {
        this.router.navigate(['/dashboard']);
      }, 2000);
      
    }).catch((error)=> {
      this.loading = false;
      console.log(error);
      this.snackBar.open(this.firebaseerror(error.code),'🤔', {
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom',
      });
    })
  }
  firebaseerror(code:string){
    switch(code){
      case'auth/wrong-password':
        return 'Contraseña incorrecta'
      case'auth/user-not-found':
        return 'Este usuario no existe'
      case'auth/invalid-email':
        return 'Correo invalido'
      default:
        return 'Error desconocido'
    }
  }
}
