import { environment } from './../../../environments/environment';
//Para logiarnos con angular
import { ReactiveFormsModule } from '@angular/forms';

//Angular materia
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input'
import { NgModule } from '@angular/core';
//Aqui exportamos y importamos materias que ofrece angular

import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
//Alerta del login
import {MatSnackBarModule} from '@angular/material/snack-bar';
//Cargador del login a ingresar
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
//Menu de opciones
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';

import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
//Referencia de API para lista de cuadrícula de material angular
import {MatGridListModule} from '@angular/material/grid-list';
//Referencia de API para selección de material angular
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
//Referencia de JSON
import { HttpClientModule } from '@angular/common/http';
//API reference for Angular Material sidenav
import {MatSidenavModule} from '@angular/material/sidenav';
//API reference for Angular Material list
import {MatListModule} from '@angular/material/list';
//Modulo de firebase
import {AngularFireModule} from '@angular/fire/compat';
//API reference for Angular Material progress-bar
import {MatProgressBarModule} from '@angular/material/progress-bar';
//importamos archivos llamando al nombre de la import

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatTableModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatCardModule,
    MatGridListModule,
    MatSelectModule,
    MatDatepickerModule,
    MatSidenavModule,
    MatProgressBarModule,
    MatListModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  //Exportamos achivos que este en otro lugar
  exports:[
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatTableModule,
    MatTooltipModule,
    MatPaginatorModule, 
    MatCardModule,
    MatProgressBarModule,
    MatGridListModule,
    MatSelectModule,
    MatDatepickerModule,
    MatSidenavModule,
    MatListModule
  ]
})
export class SharedModule { }
