import { Router} from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {AngularFireAuth} from '@angular/fire/compat/auth';

@Component({
  selector: 'app-crear-cuenta',
  templateUrl: './crear-cuenta.component.html',
  styleUrls: ['./crear-cuenta.component.css']
})
export class CrearCuentaComponent {
  registrarUsuario: FormGroup;
  loading: boolean = false;

  constructor(private fb:FormBuilder,
    private afAuth:AngularFireAuth,
    private snackBar: MatSnackBar, 
    private router: Router){
    this.registrarUsuario=this.fb.group({
      nombre:['',Validators.required],
      apellido:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
      repetirPassword:['',Validators.required],
    })
  }

  ngOnInit():void{
  }
  registrar(){
    const nombre = this.registrarUsuario.value.nombre;
    const apellido = this.registrarUsuario.value.apellido;
    const email = this.registrarUsuario.value.email;
    const password = this.registrarUsuario.value.password;
    const repetirPassword = this.registrarUsuario.value.repetirPassword;
    if(password !== repetirPassword){
      this.snackBar.open('Contraseña no coincide','❌🤡', {
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom',
      });
      return;
    }
    this.loading =true;
    this.afAuth.createUserWithEmailAndPassword(email,password).then(() => {
      this.snackBar.open('El suario fue registrado con exito','😎', {
        duration:7000,
        horizontalPosition:'center',
        verticalPosition:'top',
      });
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1500);
      
    }).catch((error)=> {
      this.loading = false;
      console.log(error);
      this.snackBar.open(this.firebaseerror(error.code),'🙃', {
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom',
      });
    })
  }
  firebaseerror(code:string){
    switch(code){
      case'auth/email-already-in-use':
        return 'El usuario ya existe'
      case'auth/weak-password':
        return 'Contraseña muy debil'
      case'auth/invalid-email':
        return 'Correo invalido'
      default:
        return 'Error desconocido'
    }
  }
}