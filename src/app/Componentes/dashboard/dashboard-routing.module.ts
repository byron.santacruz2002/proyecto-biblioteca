import { CrearLibrosComponent } from './libros/crear-libros/crear-libros.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ConversacionComponent } from './conversacion/conversacion.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { LibrosComponent } from './libros/libros.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { InicioComponent } from './inicio/inicio.component';



const routes: Routes = [
  {path:'', component:DashboardComponent, children:[
    {path:'',component:InicioComponent},
    {path:'usuarios',component:UsuariosComponent},
    {path:'crear-usuario',component:CrearUsuarioComponent},
    {path:'libros',component:LibrosComponent},
    {path:'noticias',component:NoticiasComponent},
    {path:'conversacion',component:ConversacionComponent},
    {path:'crear-libros',component:CrearLibrosComponent}
  ]} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
