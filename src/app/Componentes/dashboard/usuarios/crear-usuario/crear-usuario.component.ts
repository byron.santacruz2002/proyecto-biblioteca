import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Usuario } from 'src/app/interfaces/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
}) 
export class CrearUsuarioComponent implements OnInit {
  rol: any[] = ['Estudiante','Docente','Administrador']
  form: FormGroup;

  constructor(private fb:FormBuilder,
    private _usuarioService: UsuarioService,
    private router:Router,
    private _snackBar: MatSnackBar ){
    this.form = this.fb.group({
      nombre:['',Validators.required],
      apellido:['',Validators.required],
      telefono:['',Validators.required],
      correo:['',Validators.required],
      rol:['',Validators.required],
    })
  }
  ngOnInit(): void {
    
  }
  agregarusuario(){

    const user: Usuario = {
      nombre: this.form.value.nombre,
      apellido:this.form.value.apellido,
      telefono:this.form.value.telefono,
      correo:this.form.value.correo,
      rol: this.form.value.rol,
    }
    this._usuarioService.agregarUsuario(user);
    this.router.navigate(['/dashboard/usuarios'])
    this._snackBar.open('El usuario fue agregado con exito','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
}

