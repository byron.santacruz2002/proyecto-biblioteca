import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard.component';

import { InicioComponent } from './inicio/inicio.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { LibrosComponent } from './libros/libros.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { ConversacionComponent } from './conversacion/conversacion.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { CrearLibrosComponent } from './libros/crear-libros/crear-libros.component';


@NgModule({
  declarations: [
    InicioComponent,
    UsuariosComponent,
    LibrosComponent,
    NoticiasComponent,
    ConversacionComponent,
    NavbarComponent,
    DashboardComponent,
    CrearUsuarioComponent,
    CrearLibrosComponent
  ], 
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
