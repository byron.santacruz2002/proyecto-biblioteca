import { LibrosService } from './../../../services/libros.service';
import { Libros } from 'src/app/interfaces/libros';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit  {
  listLibros: Libros[]=[];

  displayedColumns: string[] = ['libro', 'escritor','categoria', 'ano','pdf','acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  constructor(private _librosService: LibrosService, private _snackBar: MatSnackBar){}
  //Recargamos la tabla
  ngOnInit():void{
    this.cargarLibros();
  }

  cargarLibros(){
    this.listLibros = this._librosService.getLibros();
    this.dataSource = new MatTableDataSource(this.listLibros)
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarLibros(index:number){
    console.log(index);

    this._librosService.eliminarLibro(index);
    this.cargarLibros();

    this._snackBar.open('El libro fue eliminado con exito','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
}
