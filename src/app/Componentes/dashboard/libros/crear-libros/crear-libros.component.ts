import { Libros } from 'src/app/interfaces/libros';
import { LibrosService } from './../../../../services/libros.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-crear-libros',
  templateUrl: './crear-libros.component.html',
  styleUrls: ['./crear-libros.component.css']
})
export class CrearLibrosComponent implements OnInit {
  categoria: any[] = ['Científicos','Técnicos','Instructivos','Electrónicos','Poéticos','Tecnologia']
  form: FormGroup;

  constructor(private fb:FormBuilder,
    private _librosService: LibrosService,
    private router:Router,
    private _snackBar: MatSnackBar ){
    this.form = this.fb.group({
      libro:['',Validators.required],
      escritor:['',Validators.required],
      categoria:['',Validators.required],
      ano:['',Validators.required],
      pdf:['',Validators.required],
    })
  }
  ngOnInit(): void {
    
  }
  agregarlibros(){
    const libronuevo: Libros = {
      libro: this.form.value.libro,
      escritor:this.form.value.escritor,
      categoria:this.form.value.categoria,
      ano:this.form.value.ano,
      pdf: this.form.value.pdf,
    }
    this._librosService.agregarLibro(libronuevo);
    this.router.navigate(['/dashboard/libros'])
    this._snackBar.open('El libro fue agregado con exito','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
}

