import { NoticiasService } from './../../../services/noticias.service';
import { Noticia } from './../../../interfaces/noticias';
import { Component, OnInit, ViewChild} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  listNoticia: Noticia[]=[];

  displayedColumns: string[] = ['nombre', 'descripcion','correo','fecha','acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  constructor(private _noticiaService: NoticiasService, private _snackBar: MatSnackBar){}
  //Recargamos la tabla
  ngOnInit():void{
    this.cargarNoticia();
  }

  cargarNoticia(){
    this.listNoticia = this._noticiaService.getNoticia();
    this.dataSource = new MatTableDataSource(this.listNoticia)
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarNoticia(index:number){
    console.log(index);

    this._noticiaService.eliminarNoticia(index);
    this.cargarNoticia();

    this._snackBar.open('Esta noticia fue eliminada','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
}
