import { ConversacionService } from './../../../services/conversacion.service';
import { Conversacion } from './../../../interfaces/conversacion';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-conversacion',
  templateUrl: './conversacion.component.html',
  styleUrls: ['./conversacion.component.css']
})
export class ConversacionComponent implements OnInit  {
  listConversacion: Conversacion[]=[];

  displayedColumns: string[] = ['fecha', 'usuario','comentario'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  constructor(private _conversacionService: ConversacionService, private _snackBar: MatSnackBar){}
  //Recargamos la tabla
  ngOnInit():void{
    this.cargarConversacion();
  }

  cargarConversacion(){
    this.listConversacion = this._conversacionService.getConversacion();
    this.dataSource = new MatTableDataSource(this.listConversacion)
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarConversacion(index:number){
    console.log(index);

    this._conversacionService.eliminarConversacion(index);
    this.cargarConversacion();

    this._snackBar.open('Esta conversacion fue eliminada','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
}
