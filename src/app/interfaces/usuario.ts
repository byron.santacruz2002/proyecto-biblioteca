export interface Usuario{
    $key?:string;//Angular necesita este campo.
    nombre:string;
    apellido:string;
    telefono:number;
    correo: string;
    rol:string;
}