export interface Conversacion{
    $key?:string;//Angular necesita este campo.
    fecha:string;
    usuario:string;
    comentario:string;
}