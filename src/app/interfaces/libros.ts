export interface Libros{
    $key?:string;//Angular necesita este campo.
    libro:string;
    escritor:string;
    categoria:string;
    ano: string;
    pdf:string;
}