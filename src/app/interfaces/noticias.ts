export interface Noticia{
    $key?:string;//Angular necesita este campo.
    nombre:string;
    descripcion:string;
    correo:string;
    fecha:string;
}