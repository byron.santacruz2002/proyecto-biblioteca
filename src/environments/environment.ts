// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'proyecto-50962',
    appId: '1:677608961896:web:ed6e5dd0dc8fcf30fa08dd',
    storageBucket: 'proyecto-50962.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyDI41QHhrwcdUmZHXMBk6NOnU5Z6OFDrEg',
    authDomain: 'proyecto-50962.firebaseapp.com',
    messagingSenderId: '677608961896',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
